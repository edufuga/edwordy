package com.gitlab.edufuga.wordlines.edwordybot

import com.gitlab.edufuga.wordlines.core.Status
import com.gitlab.edufuga.wordlines.core.WordStatusDate
import com.gitlab.edufuga.wordlines.recordwriter.RecordWriterService
import com.gitlab.edufuga.wordlines.sentenceparser.SentenceParserService
import groovy.transform.TypeChecked
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.exceptions.TelegramApiException

import java.util.concurrent.ConcurrentLinkedQueue

@TypeChecked
class EdwordyBot extends TelegramLongPollingBot {
    private final String botUserName
    private final String botToken

    private SentenceParserService sentenceParserService = new SentenceParserService()
    private RecordWriterService recordWriterService = new RecordWriterService()

    private Map<User, WordStatusDate> currentRecordForUser = new HashMap<>()
    private Map<User, Queue<WordStatusDate>> recordsForUser = new HashMap<>()

    private Map<User, String> currentLanguageForUser = new HashMap<>()

    private Map<User, List<Status>> statesForUser = new HashMap<>()

    EdwordyBot(String botUserName, String botToken) {
        this.botUserName = botUserName
        this.botToken = botToken
    }

    @Override
    String getBotUsername() {
        return botUserName
    }

    @Override
    String getBotToken() {
        return botToken
    }

    @Override
    void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {

            // Get the original text.
            String originalText = update.getMessage().getText()
            println "You entered: $originalText"

            User user = update.getMessage().getFrom()
            Long userId = user.getId()
            println "You are: $userId"

            // Distinguish commands (messages start with "/")
            if (originalText.startsWith("/")) {
                println "You have entered a command."

                if (originalText.startsWith("/language")) {
                    String language = originalText.replace("/language", "").trim()
                    println "Language: $language"
                    currentLanguageForUser.put(user, language)

                    SendMessage message = new SendMessage()
                    message.setChatId(update.getMessage().getChatId().toString())
                    message.setText("I've set the current language to $language")
                    try {
                        execute(message)
                    } catch (TelegramApiException e) {
                        e.printStackTrace()
                    }
                }
                else if (originalText.startsWith("/states")) {
                    String states = originalText.replace("/states", "").trim()
                    println "States: $states"
                    List<Status> singleStatesContants
                    try {
                        List<String> singleStates = Arrays.asList(states.split(", "))
                        singleStatesContants = singleStates.collect { s -> Status.valueOf(s) }
                    }
                    catch (Exception ignored)
                    {
                        singleStatesContants = [Status.NEW, Status.UNKNOWN, Status.GUESSED]
                    }
                    statesForUser.put(user, singleStatesContants)

                    SendMessage message = new SendMessage()
                    message.setChatId(update.getMessage().getChatId().toString())
                    message.setText("I've set the current states to $singleStatesContants")
                    try {
                        execute(message)
                    } catch (TelegramApiException e) {
                        e.printStackTrace()
                    }
                }
            }
            else {
                // Use the current language of the user, or English as default.
                String language = currentLanguageForUser.getOrDefault(user, "EN")

                // Split into records.
                println "Parsing the sentence..."
                List<WordStatusDate> wordsInSentence = sentenceParserService.parseSentence(language, originalText)

                if (wordsInSentence == null || wordsInSentence.isEmpty()) {
                    println "No records found in sentence $originalText"
                    return
                }

                // Filter records for the wanted states
                println "Number of words in sentence: " + wordsInSentence.size()

                // Consider only unique words (i.e. without duplicates)
                wordsInSentence = wordsInSentence.unique()

                println "Number of unique words in sentence: " + wordsInSentence.size()
                println wordsInSentence

                // Keep only the words with the initial state in the wanted whitelist
                List<Status> wantedStates = statesForUser.get(user)
                wordsInSentence = wordsInSentence.findAll { r -> wantedStates.contains(r.getStatus()) }
                println "Number of words in the status whitelist: " + wordsInSentence.size()

                println "Transforming list to a queue"
                recordsForUser.put(user, new ConcurrentLinkedQueue<>(wordsInSentence))
                Queue<WordStatusDate> records = recordsForUser.get(user)

                println "Number of records: " + records.size()

                // Question the first record
                if (records.isEmpty()) {
                    SendMessage message = new SendMessage()
                    message.setChatId(update.getMessage().getChatId().toString())
                    message.setText("Nothing to do... Sadge.")

                    try {
                        execute(message)
                    } catch (TelegramApiException e) {
                        e.printStackTrace()
                    }
                }
                else {
                    // Get the first record (this will be *written* once the callback is called)
                    currentRecordForUser.put(user, records.poll())
                    WordStatusDate currentRecord = currentRecordForUser.get(user)

                    SendMessage message = new SendMessage()
                    message.setChatId(update.getMessage().getChatId().toString())
                    message.setText(currentRecord.toString() + "  (" + language + ")")
                    message.setReplyMarkup(createKeyboard())

                    try {
                        execute(message)
                    } catch (TelegramApiException e) {
                        e.printStackTrace()
                    }
                }
            }
        }
        else if (update.hasCallbackQuery()) {
            String callbackData = update.getCallbackQuery().getData()

            println "Callback from " + update.getCallbackQuery().getFrom()

            User user = update?.getCallbackQuery()?.getFrom()
            println "You are (callback): $user"

            if (user == null) {
                return
            }

            String language = currentLanguageForUser.getOrDefault(user, "EN")
            println "Current language: $language"

            // Question the remaining records
            Queue<WordStatusDate> records = recordsForUser.getOrDefault(user, new ConcurrentLinkedQueue<WordStatusDate>())
            println "Number of records before poll: " + records.size()
            if (records.isEmpty()) {
                reattachEmptyKeyboard(update, "Done! 🎉🎉")
                answer(update, "Done for now!")
            }

            // Get the previously set record
            WordStatusDate currentRecord = currentRecordForUser.get(user)
            println "Current record: $currentRecord"

            if (callbackData == "unknown_msg_text") {
                answer(update, "So you don't know that word?")
                writeRecord(language, currentRecord, Status.UNKNOWN)
                askNextWord(update)
            }
            else if (callbackData == "ignored_msg_text") {
                answer(update, "So you ignore that word?")
                writeRecord(language, currentRecord, Status.IGNORED)
                askNextWord(update)
            }
            else if (callbackData == "guessed_msg_text") {
                answer(update, "So you just guessed that word?")
                writeRecord(language, currentRecord, Status.GUESSED)
                askNextWord(update)
            }
            else if (callbackData == "comprehended_msg_text") {
                answer(update, "So you understand that word?")
                writeRecord(language, currentRecord, Status.COMPREHENDED)
                askNextWord(update)
            }
            else if (callbackData == "known_msg_text") {
                answer(update, "So you actively know that word?")
                writeRecord(language, currentRecord, Status.KNOWN)
                askNextWord(update)
            }
            else if (callbackData == "mastered_msg_text") {
                answer(update, "Sure you won't forget it?")
                writeRecord(language, currentRecord, Status.MASTERED)
                askNextWord(update)
            }
            else if (callbackData == "jump_msg_text") {
                answer(update, "So you want to jump this...")
                askNextWord(update)
            }
            else if (callbackData == "cancel_msg_text") {
                answer(update, "OMG!")
                cancelEverything(update)
            }
        }
    }

    private void writeRecord(String language, WordStatusDate record, Status status) {
        if (record == null) {
            return
        }
        try {
            recordWriterService.writeRecord(language, record.getWord(), status.toString())
        }
        catch (Exception e) {
            e.printStackTrace()
        }
    }

    private void askNextWord(Update update) {
        User user = update?.getCallbackQuery()?.getFrom()

        if (user == null) {
            return
        }

        println "Let's ask $user.firstName another word..."

        String language = currentLanguageForUser.getOrDefault(user, "EN")

        Queue<WordStatusDate> records = recordsForUser.getOrDefault(user, new ConcurrentLinkedQueue<WordStatusDate>())

        println "Records size: " + records.size()

        if (records.isEmpty()) {
            reattachEmptyKeyboard(update, "Done! 🎉")
            answer(update, "Done for now!")
        }
        else {
            println "Updating record for the next callback..."
            currentRecordForUser.put(user, records.poll())
            WordStatusDate currentRecord = currentRecordForUser.get(user)
            println "Current record: $currentRecord"
            if (currentRecord == null) {
                reattachEmptyKeyboard(update, "Current record is empty?")
                answer(update, "Current record is empty?")
            }
            else {
                println "Let's ask $user.firstName the word $currentRecord"
                reattachKeyboard(update, currentRecord.toString() + " (" + language + ")")
            }
        }
    }

    private void cancelEverything(Update update) {
        User user = update?.getCallbackQuery()?.getFrom()

        if (user == null) {
            return
        }

        recordsForUser.put(user, new ConcurrentLinkedQueue<WordStatusDate>())
        try {
            currentRecordForUser.remove(user)
        }
        catch (Exception ignored) {

        }

        reattachEmptyKeyboard(update, "Everything cleared up.")
        answer(update, "Done for now...")
    }

    private void answer(Update update, String text) {
        AnswerCallbackQuery answerCallback = answerCallback(update, text)
        try {
            execute(answerCallback)
        } catch (TelegramApiException e) {
            e.printStackTrace()
        }
    }

    private void reattachKeyboard(Update update, String text="Let's try once more...") {
        EditMessageText editMessage = editCallback(update, text)
        editMessage.setReplyMarkup(createKeyboard())

        try {
            execute(editMessage)
        } catch (TelegramApiException e) {
            e.printStackTrace()
        }
    }

    private void reattachEmptyKeyboard(Update update, String text="Clearing the keyboard...") {
        EditMessageText editMessage = editCallback(update, text)
        editMessage.setReplyMarkup(emptyKeyboard())

        try {
            execute(editMessage)
        } catch (TelegramApiException e) {
            e.printStackTrace()
        }
    }

    private static EditMessageText edit(Update update, String text) {
        EditMessageText message = new EditMessageText()
        message.setChatId(update.getMessage().getChatId().toString())
        message.setMessageId(update.getMessage().getMessageId())
        message.setText(text)
        return message
    }

    private static EditMessageText editCallback(Update update, String text) {
        EditMessageText message = new EditMessageText()
        message.setChatId(update.getCallbackQuery().getMessage().getChatId().toString())
        message.setMessageId(update.getCallbackQuery().getMessage().getMessageId())
        message.setText(text)
        return message
    }

    private static AnswerCallbackQuery answerCallback(Update update, String text) {
        AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery()
        answerCallbackQuery.setCallbackQueryId(update.getCallbackQuery().getId())
        answerCallbackQuery.setText(text)
        return answerCallbackQuery
    }

    private static InlineKeyboardMarkup createKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup()

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>()

        // Zeroth row
        List<InlineKeyboardButton> zeroRow = new ArrayList<>()

        InlineKeyboardButton jumpButton = new InlineKeyboardButton()
        jumpButton.setText("JUMP")
        jumpButton.setCallbackData("jump_msg_text")

        InlineKeyboardButton cancelButton = new InlineKeyboardButton()
        cancelButton.setText("CANCEL")
        cancelButton.setCallbackData("cancel_msg_text")

        zeroRow.add(jumpButton)
        zeroRow.add(cancelButton)

        rowsInline.add(zeroRow)

        // First row
        List<InlineKeyboardButton> firstRow = new ArrayList<>()

        InlineKeyboardButton unknownButton = new InlineKeyboardButton()
        unknownButton.setText("UNKNOWN")
        unknownButton.setCallbackData("unknown_msg_text")

        InlineKeyboardButton ignoredButton = new InlineKeyboardButton()
        ignoredButton.setText("IGNORED")
        ignoredButton.setCallbackData("ignored_msg_text")

        firstRow.add(unknownButton)
        firstRow.add(ignoredButton)

        rowsInline.add(firstRow)

        // Second row
        List<InlineKeyboardButton> secondRow = new ArrayList<>()

        InlineKeyboardButton guessedButton = new InlineKeyboardButton()
        guessedButton.setText("GUESSED")
        guessedButton.setCallbackData("guessed_msg_text")

        InlineKeyboardButton comprehendedButton = new InlineKeyboardButton()
        comprehendedButton.setText("COMPREHENDED")
        comprehendedButton.setCallbackData("comprehended_msg_text")

        secondRow.add(guessedButton)
        secondRow.add(comprehendedButton)

        rowsInline.add(secondRow)

        // Third row
        List<InlineKeyboardButton> thirdRow = new ArrayList<>()

        InlineKeyboardButton knownButton = new InlineKeyboardButton()
        knownButton.setText("KNOWN")
        knownButton.setCallbackData("known_msg_text")

        InlineKeyboardButton masteredButton = new InlineKeyboardButton()
        masteredButton.setText("MASTERED")
        masteredButton.setCallbackData("mastered_msg_text")

        thirdRow.add(knownButton)
        thirdRow.add(masteredButton)

        rowsInline.add(thirdRow)

        // Set the keyboard
        inlineKeyboardMarkup.setKeyboard(rowsInline)

        return inlineKeyboardMarkup
    }

    private static InlineKeyboardMarkup emptyKeyboard() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup()

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>()

        List<InlineKeyboardButton> firstRow = new ArrayList<>()

        rowsInline.add(firstRow)

        inlineKeyboardMarkup.setKeyboard(rowsInline)

        return inlineKeyboardMarkup
    }
}
